# Easy importing of csv files
import csv

# Good for printing out dictionaries
import json

# SUMMARY:
# - Input(s):
# --- "filename" -- The name of the file we should look through.
# - Output:
# --- The average account balance for customers in each marital status category.
# - Output Type:
# --- Type:
# ----- Dict
# --- Keys:
# ----- "Balance (AVG)" -- The average balance across all customers.
# ----- "Num in Category" -- The number of people whose data has been looked at.
def parse_loans(filename):

    # FILE FORMAT:
    # [0] age, [1] job, [2] marital, [3] education, [4] default,
    # [5] balance, [6] housing, [7] loan, [8] contact, [9] day,
    # [10] month, [11] duration, [12] campaign, [13] pdays, [14] previous,
    # [15] poutcome, [16] approved

    # Open loans file.
    with open(filename, "r") as f:

        # Prepare the file info for enumeration.
        lines = csv.reader(f)

        # We will store marriage statuses and their balance averages here.
        marriage_statuses = {}

        for num, line in enumerate(lines):

            # Skip header row.
            if num == 0:
                continue

            # Get relevant fields.
            marriage_status = line[2]

            # Make sure the balance actually comes in as a number not a string.
            balance = float(line[5])

            # If we haven't looked at this marriage status yet, make a base instance.
            if marriage_status not in marriage_statuses.keys():

                marriage_statuses[marriage_status] = {
                    "Balance (AVG)": balance,
                    "Num in Category": 1
                }

            else:

                current_entry = marriage_statuses[marriage_status]

                # Multiply the current average by num in category, add this new balance,
                # then divide by num in category + 1 to get a new average.
                new_average = (
                        ((current_entry["Balance (AVG)"] * current_entry["Num in Category"]) + balance)
                        / (current_entry["Num in Category"] + 1)
                )

                # Replace the values in the entry with the new values.
                current_entry["Balance (AVG)"] = new_average
                current_entry["Num in Category"] += 1

        return marriage_statuses


if __name__ == "__main__":

    parsed_loans = parse_loans("loans.csv")
    print(json.dumps(parsed_loans, indent=2))

